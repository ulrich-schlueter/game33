package backend

type iEnemy interface {
	tick()
	Context() *Context
	DecreaseLives(int32) int32
}

type iTower interface {
	tick()
	Context() *Context
}

type Context struct {
	Location     Coordinate
	Lifetime     int32
	Size         float64
	Strength     int32
	ShotStrength int32
	IsAlive      bool
}

package frontend

import (
	"fmt"
	"image/color"
	_ "image/png"
	"log"

	be "gitlab.com/ulrichschlueter/game33/pkg/backend"

	"github.com/hajimehoshi/bitmapfont/v2"

	"github.com/hajimehoshi/ebiten/v2"
	eu "github.com/hajimehoshi/ebiten/v2/ebitenutil"
	"github.com/hajimehoshi/ebiten/v2/inpututil"
	"github.com/hajimehoshi/ebiten/v2/text"
)

var (
	emptyImage = ebiten.NewImage(3, 3)
)

func init() {
	emptyImage.Fill(color.White)
}

const (
	screenWidth  = 800
	screenHeight = 800
)

func drawPaths(screen *ebiten.Image, g *be.Game) {

	//cellSize := float32(board.CellSizePx)
	for _, p := range g.Paths {

		cx := float64(p.StartPoint.X)
		cy := float64(p.StartPoint.Y)
		for _, d := range p.Deltas {

			nx := cx + float64(d.X)
			ny := cy + float64(d.Y)
			eu.DrawLine(screen, cx, cy, nx, ny, color.Black)
			cx = nx
			cy = ny
		}

	}

	// Paint the shape with a black color.
}

func DrawImageAt(screen *ebiten.Image, g *be.Game, name string, x float64, y float64) {
	img := g.Images[name]
	isize := img.Bounds().Max

	cx := float64(x)
	cy := float64(y)
	op := &ebiten.DrawImageOptions{}
	//op.GeoM.Scale(m.Size/10, m.Size/10)
	op.GeoM.Translate(cx-float64(isize.X)/2, cy-float64(isize.Y)/2)

	op.ColorM.Scale(1.0, 1.0, 1.0, 0.5)
	screen.DrawImage(img, op)
}

func drawMovingParts(screen *ebiten.Image, g *be.Game) {

	for _, m := range g.Monsters {
		c := m.Context()
		DrawImageAt(screen, g, "monster", float64(c.Location.X), float64(c.Location.Y))
		label := fmt.Sprintf("%d", c.Lifetime)
		text.Draw(screen, label, bitmapfont.Face, int(c.Location.X)+50, int(c.Location.Y)+20, color.Black)

	}

}

func drawFixparts(screen *ebiten.Image, g *be.Game) {
	//dc := g.GGContext

	for _, f := range g.Towers {
		c := f.Context()
		cx := float64(c.Location.X)
		cy := float64(c.Location.Y)

		d := c.Size / 2
		eu.DrawRect(screen, cx-d, cy-d, d*2, d*2, color.Black)
		DrawImageAt(screen, g, "circle50", float64(cx), float64(cy))
		DrawImageAt(screen, g, "circle100", float64(cx), float64(cy))
		//clr := color.RGBA{0x80, 0x80, 0x80, 0x80}

	}

}

//func maxCounter(index int) int {
//	return 128 + (17*index+32)%64
//}

type UI struct {
	counter int
	*be.Game
}

func (g *UI) Update() error {
	g.counter++

	mx, my := ebiten.CursorPosition()
	if inpututil.IsMouseButtonJustReleased(ebiten.MouseButtonLeft) {
		var newTower = be.NewGrabTower(be.Coordinate{X: float64(mx), Y: float64(my)}, 20, 5.0)
		g.Game.Towers = append(g.Game.Towers, newTower)
	}

	return nil
}

func BenchMarkDraw(screen *ebiten.Image, g *UI) {
	g.GameTick()
	screen.Fill(color.White)
	drawPaths(screen, g.Game)
	drawMovingParts(screen, g.Game)
	drawFixparts(screen, g.Game)
	eu.DebugPrint(screen, fmt.Sprintf("TPS: %0.2f\nFPS: %0.2f", ebiten.CurrentTPS(), ebiten.CurrentFPS()))
}

func (g *UI) Draw(screen *ebiten.Image) {
	BenchMarkDraw(screen, g)
}

func (g *UI) Layout(outsideWidth, outsideHeight int) (int, int) {
	return screenWidth, screenHeight
}

func genCircleImage(c int) (img *ebiten.Image) {
	img = ebiten.NewImage(c, c)
	drawCircle(img, c/2, c/2, c/2, color.Black)

	return
}

func RunGame() {
	g := &UI{counter: 0}
	g.Game = be.NewGame(800, 800)
	var img *ebiten.Image
	img, _, err := eu.NewImageFromFile("../assets/monster.png")
	if err != nil {
		log.Fatal(err)
	}
	g.Images["monster"] = img
	g.Images["circle50"] = genCircleImage(50)
	g.Images["circle100"] = genCircleImage(100)

	ebiten.SetWindowSize(screenWidth, screenHeight)
	ebiten.SetWindowTitle("Game 33")
	if err := ebiten.RunGame(g); err != nil {
		log.Fatal(err)
	}
}

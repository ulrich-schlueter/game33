package backend

type GrabTower struct {
	tower Tower
}

func NewGrabTower(c Coordinate, lifetime int, size float64) *GrabTower {
	return &GrabTower{
		tower: Tower{
			context: Context{
				Location:     c,
				Lifetime:     int32(lifetime),
				Size:         size,
				IsAlive:      true,
				Strength:     50,
				ShotStrength: 1,
			},
		},
	}

}

func (g *GrabTower) tick() {

}

func (g *GrabTower) Context() *Context {
	return &g.tower.context

}

package main

import "fmt"

type iInterface interface {
	A()
}

type Interface struct {
	a int
}

type I1 struct {
	i Interface
	b int
}

func (i *I1) A() {
	fmt.Println("I1")
}

func NewI1() iInterface {
	return &I1{
		i: Interface{
			a: 0,
		},
		b: 1,
	}

}

type I2 struct {
	i Interface
	b int
}

func (i *I2) A() {
	fmt.Println("I2")
}
func NewI2() iInterface {
	return &I2{
		i: Interface{
			a: 0,
		},
		b: 1,
	}

}

func main() {
	m := []iInterface{}
	m = append(m, NewI1())
	m = append(m, NewI2())

	for _, x := range m {
		x.A()
	}
}

module gitlab.com/ulrichschlueter/game33

go 1.16

require (
	github.com/fogleman/gg v1.3.0
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/hajimehoshi/bitmapfont/v2 v2.1.3
	github.com/hajimehoshi/ebiten/v2 v2.2.1
	golang.org/x/sys v0.0.0-20211109065445-02f5c0300f6e // indirect
)

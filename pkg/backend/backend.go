package backend

import (
	"math"

	"github.com/fogleman/gg"
	"github.com/hajimehoshi/ebiten/v2"
)

type Cell struct {
	Value    int
	NewValue int
	Overlay  string
}

type Coordinate struct {
	X float64
	Y float64
}

func NewCoordinate(x float64, y float64) *Coordinate {
	return &Coordinate{
		X: x,
		Y: y,
	}

}

type Vector2D struct {
	X float64
	Y float64
}

func NewVector2D(x float64, y float64) *Vector2D {
	return &Vector2D{
		X: x,
		Y: y,
	}

}

type Path struct {
	StartPoint Coordinate
	Deltas     []Vector2D
}

func NewPath() *Path {
	return &Path{}
}

func (p *Path) closePath() {
	dx := float64(0.0)
	dy := float64(0.0)
	for _, d := range p.Deltas {
		dx += d.X
		dy += d.Y
	}
	if dx != 0.0 || dy != 0.0 {
		p.Deltas = append(p.Deltas, *NewVector2D(-dx, -dy))
	}
}

type Game struct {
	Width  int
	Height int

	BoardCells       [][]Cell
	Paths            []*Path
	Monsters         []iEnemy
	Towers           []iTower
	Images           map[string]*ebiten.Image
	GGContext        *gg.Context
	RespawnTick      int
	RespawnCountDown int
}

func NewGame(x, y int) (g *Game) {

	g = &Game{
		Width:            x,
		Height:           y,
		Images:           make(map[string]*ebiten.Image),
		RespawnTick:      10,
		RespawnCountDown: 10,
	}
	rows := make([][]Cell, g.Height)
	for i := range rows {
		rows[i] = make([]Cell, g.Width)
	}
	g.BoardCells = rows

	g.AddPaths()
	g.AddMovingParts()
	return
}

func (g *Game) getTower(mx float64, my float64) iTower {

	return NewGrabTower(Coordinate{X: float64(mx), Y: float64(my)}, 20, 5.0)
}

func (g *Game) GameTick() {

	for _, t := range g.Towers {
		t.tick()
	}

	for _, m := range g.Monsters {
		m.tick()
	}

	g.RespawnCountDown -= 1
	if g.RespawnCountDown < 0 {
		g.RespawnCountDown = g.RespawnTick
		g.Monsters = append(g.Monsters, NewMonster(g.Paths[0], 3.0, 30, 100))
	}

	g.postMoveProcessing()
}

func (g *Game) postMoveProcessing() {
	var fa []iTower

	for _, f := range g.Towers {
		c := f.Context()
		if c.Lifetime > 0 {
			fa = append(fa, f)
		}
		ts := f.Context().Strength
		ss := f.Context().ShotStrength
		for _, m := range g.Monsters {
			distX := m.Context().Location.X - c.Location.X
			distY := m.Context().Location.Y - c.Location.Y
			dist := math.Sqrt(distX*distX + distY*distY)
			if dist < 100 && ts > 0 {
				m.DecreaseLives(ss)
				ts -= ss
			}
		}
	}
	g.Towers = fa

	var ma []iEnemy
	for _, m := range g.Monsters {
		if m.Context().Lifetime > 0 {
			ma = append(ma, m)
		}
	}
	g.Monsters = ma
}

func (g *Game) AddPaths() {

	d := []Vector2D{*NewVector2D(0, 100),
		*NewVector2D(150, 0),
		*NewVector2D(0, 100),
		*NewVector2D(-50, 0),
		*NewVector2D(300, 0),
		*NewVector2D(0, 400),
	}
	p := NewPath()
	p.StartPoint = *NewCoordinate(50, 50)
	p.Deltas = d
	p.closePath()
	g.Paths = append(g.Paths, p)

	p = NewPath()
	p.StartPoint = *NewCoordinate(50, 55)
	p.Deltas = d
	p.closePath()
	g.Paths = append(g.Paths, p)

	p = NewPath()
	p.StartPoint = *NewCoordinate(60, 55)
	p.Deltas = d
	p.closePath()
	g.Paths = append(g.Paths, p)

	p = NewPath()
	p.StartPoint = *NewCoordinate(55, 55)
	p.Deltas = d
	g.Paths = append(g.Paths, p)

	p = NewPath()
	p.StartPoint = *NewCoordinate(200, 300)
	p.Deltas = append(p.Deltas, *NewVector2D(10, 10), *NewVector2D(289, 20), *NewVector2D(100, 100))
	p.closePath()
	g.Paths = append(g.Paths, p)
}

func (g *Game) AddMovingParts() {

	g.Monsters = append(g.Monsters, NewMonster(g.Paths[0], 4.0, 5, 100))
	g.Monsters = append(g.Monsters, NewMonster(g.Paths[1], 4.0, 10, 100))
	g.Monsters = append(g.Monsters, NewMonster(g.Paths[0], 3.0, 30, 100))
	g.Monsters = append(g.Monsters, NewMonster(g.Paths[3], 3.0, 1, 100))
	g.Monsters = append(g.Monsters, NewMonster(g.Paths[4], 3.0, 0.5, 100))

}

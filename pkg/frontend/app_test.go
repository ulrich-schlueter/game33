package frontend

import (
	"log"
	"testing"

	"github.com/hajimehoshi/ebiten/v2"
	eu "github.com/hajimehoshi/ebiten/v2/ebitenutil"
	be "gitlab.com/ulrichschlueter/game33/pkg/backend"
)

func TestBenckmarkOne(b *testing.T) {
	g := &UI{counter: 0}
	g.Game = be.NewGame(800, 800)
	var img *ebiten.Image
	img, _, err := eu.NewImageFromFile("../../assets/monster.png")
	if err != nil {
		log.Fatal(err)
	}
	g.Images["monster"] = img

	ebiten.SetWindowSize(screenWidth, screenHeight)
	ebiten.SetWindowTitle("Game 33")
	BenchMarkDraw(ebiten.NewImage(1000, 1000), g)
}

package backend

import "math"

type Monster struct {
	path          *Path
	nextpathIndex int
	context       Context
	LastMidPoint  Coordinate
	speed         float64
}

func NewMonster(p *Path, speed float64, size float64, life int32) *Monster {
	return &Monster{
		path:          p,
		nextpathIndex: 0,
		context: Context{
			Location:     p.StartPoint,
			Size:         size,
			Lifetime:     life,
			Strength:     5,
			ShotStrength: 1},
		LastMidPoint: p.StartPoint,
		speed:        speed,
	}

}

func (m *Monster) Context() *Context {
	return &m.context
}

func (m *Monster) DecreaseLives(i int32) (newLives int32) {
	m.context.Lifetime -= i
	if m.context.Lifetime < 0 {
		m.context.IsAlive = false
	}
	newLives = m.context.Lifetime
	return
}

func (m *Monster) tick() {
	nextStop := m.path.Deltas[m.nextpathIndex]

	tx := float64(m.LastMidPoint.X + nextStop.X)
	ty := float64(m.LastMidPoint.Y + nextStop.Y)
	dx := tx - float64(m.context.Location.X)
	dy := ty - float64(m.context.Location.Y)
	ndx := float64(0)
	ndy := float64(0)
	v := math.Sqrt(dx*dx + dy*dy)
	if v < 4 {
		m.LastMidPoint.X = m.LastMidPoint.X + nextStop.X
		m.LastMidPoint.Y = m.LastMidPoint.Y + nextStop.Y
		m.nextpathIndex = m.nextpathIndex + 1
		if m.nextpathIndex >= len(m.path.Deltas) {
			m.nextpathIndex = 0
			m.context.Location = m.path.StartPoint
			m.LastMidPoint = m.path.StartPoint
		}
	} else {

		ndx = dx / v * m.speed
		ndy = dy / v * m.speed
	}

	m.context.Location.X = m.context.Location.X + ndx
	m.context.Location.Y = m.context.Location.Y + ndy

}
